#!/bin/bash

#################################
eval "$(conda shell.bash hook)"
conda activate SQANTI3.env

#################################################
## Download raw data first from GEO: 

#GSM6380406	isoseq_naive_WT
#GSM6380407	isoseq_48H_activated_noDiv_WT
#GSM6380408	isoseq_48H_activated_2Div_W

sample="isoseq_naive_WT"  

OUTDIR=""  ##Define outdir

mkdir -p $OUTDIR

##Define PATHs
lima_PATH="lima"
SQANTI3_PATH="SQANTI3"
cDNA_CupcakePATH="cDNA_Cupcake"
GTF="ref/gencode.vM25.annotation.sorted.noChr.gtf" 
tappasLike_GFF3="ref/PacBio/Mus_musculus_GRCm38_Ensembl_86_tappasLike.gff3"
GENOME="ref/Mus_musculus.GRCm38.dna.toplevel.fa"
PRIMERS="ref/PacBio/primers.fasta"
CAGE="ref/PacBio/mm10_fair+new_CAGE_peaks_phase1and2.noChr.bed"
POLYA_MOTIF="ref/PacBio/mouse_GRCm38_96_polyA_motifs.txt"
POLYA_PEAK="ref/PacBio/polyAsiteOfinterest_polyAminer.bed"

echo "###########################"
echo "Isoseq analysis of "$sample
echo "###########################"

echo "1. Generate consensus sequences from your raw subread data"
ccs --min-rq 0.9 --report-file ${OUTDIR}/${sample}.ccs.report.txt --log-file ${OUTDIR}/${sample}.ccs.report.log --log-level TRACE ${OUTDIR}/${sample}.subreads.bam ${OUTDIR}/${sample}.ccs.bam

echo "2. Generate full-length reads by primer removal and demultiplexing"
lima --ccs --log-file ${OUTDIR}/${sample}.lima.report.log --log-level TRACE --dump-removed ${OUTDIR}/${sample}.ccs.bam $PRIMERS ${OUTDIR}/${sample}.ccs.fl.bam
mkdir -p ${OUTDIR}/lima_reports
Rscript --vanilla ${lima_PATH}/report_detail.R ${OUTDIR}/${sample}.ccs.fl.lima.report pdf ${OUTDIR}/lima_reports/
Rscript --vanilla ${lima_PATH}/report_summary.R ${OUTDIR}/${sample}.ccs.fl.lima.report ${OUTDIR}/lima_reports/

echo "3. Remove polyA tails and artificial concatemers"
isoseq3 refine --require-polya --min-polya-length 20 ${OUTDIR}/${sample}.ccs.fl.bam $PRIMERS ${OUTDIR}/${sample}.ccs.flnc.bam  

echo "4. Cluster consensus sequences to generate transcriptome fastas"
isoseq3 cluster ${OUTDIR}/${sample}.ccs.flnc.bam ${OUTDIR}/${sample}.ccs.flnc.clustered.bam --verbose --use-qvs

echo "5. Map High-Quality Full-Length Polished Isoforms to genome and collapse transcripts based on genomic mapping"
pbmm2 align --log-file ${OUTDIR}/${sample}.pbmm2-align.log --log-level TRACE --preset ISOSEQ --bam-index BAI --sort $GENOME ${OUTDIR}/${sample}.ccs.flnc.clustered.bam ${OUTDIR}/${sample}.ccs.flnc.clustered.aligned.sorted.bam
isoseq3 collapse --log-file ${OUTDIR}/${sample}.ccs.flnc.clustered.aligned.sorted.collapsed.fastq.log --log-level TRACE ${OUTDIR}/${sample}.ccs.flnc.clustered.aligned.sorted.bam ${OUTDIR}/${sample}.ccs.bam ${OUTDIR}/${sample}.ccs.flnc.clustered.aligned.sorted.collapsed.rep.fastq
isoseq3 collapse --log-file ${OUTDIR}/${sample}.ccs.flnc.clustered.aligned.sorted.collapsed.gff.log --log-level TRACE ${OUTDIR}/${sample}.ccs.flnc.clustered.aligned.sorted.bam ${OUTDIR}/${sample}.ccs.bam ${OUTDIR}/${sample}.ccs.flnc.clustered.aligned.sorted.collapsed.gff
gffread ${OUTDIR}/${sample}.ccs.flnc.clustered.aligned.sorted.collapsed.gff -T -o ${OUTDIR}/${sample}.ccs.flnc.clustered.aligned.sorted.collapsed.gtf
cp ${OUTDIR}/${sample}.ccs.flnc.clustered.aligned.sorted.collapsed.rep.fasta ${OUTDIR}/${sample}.ccs.flnc.clustered.aligned.sorted.collapsed.rep.fa

echo "6. Run SQANTI3 annotation"
mkdir -p ${OUTDIR}/SQANTI3_2
python ${SQANTI3_PATH}/sqanti3_qc.py -o ${sample} -g --aligner_choice minimap2 -d ${OUTDIR}/SQANTI3_2  --polyA_motif_list ${POLYA_MOTIF} -fl ${OUTDIR}/${sample}.ccs.flnc.clustered.aligned.sorted.collapsed.abundance.txt --cage_peak $CAGE --genename ${OUTDIR}/${sample}.ccs.flnc.clustered.aligned.sorted.collapsed.gtf $GTF $GENOME

echo "7. Run isoAnnoLite to annotate gff3 of ensembl version 94"
python ${SQANTI3_PATH}/utilities/IsoAnnotLite_SQ3.py -gff3 ${tappasLike_GFF3} -d ${OUTDIR}/SQANTI3_2 -o ${sample} ${OUTDIR}/SQANTI3_2/${sample}_corrected.gtf ${OUTDIR}/SQANTI3_2/${sample}_classification.txt ${OUTDIR}/SQANTI3_2/${sample}_junctions.txt

echo "8. Run again SQANTI3 annotation with isoAnnotLite option and tappasLike gff3"
python ${SQANTI3_PATH}/sqanti3_qc.py -o ${sample} -g --aligner_choice minimap2 -d ${OUTDIR}/SQANTI3_2 --polyA_motif_list ${POLYA_MOTIF} -fl ${OUTDIR}/${sample}.ccs.flnc.clustered.aligned.sorted.collapsed.abundance.txt --cage_peak $CAGE --genename --isoAnnotLite --gff3 ${OUTDIR}/SQANTI3_2/${sample}_tappAS_annot_from_SQANTI3.gff3 ${OUTDIR}/${sample}.ccs.flnc.clustered.aligned.sorted.collapsed.gtf $GTF $GENOME

echo "9. Run DNA_cupcake rarefaction" # scripts https://github.com/Magdoll/cDNA_Cupcake/wiki/Annotation-and-Rarefaction-Wiki
mkdir -p ${OUTDIR}/cDNA_Cupcake
python ${cDNA_CupcakePATH}/annotation/make_file_for_subsampling_from_collapsed.py -i ${OUTDIR}/${sample}.ccs.flnc.clustered.aligned.sorted.collapsed -o ${OUTDIR}/cDNA_Cupcake/${sample}.for_subsampling.txt -m2 ${OUTDIR}/SQANTI3_2/${sample}_classification.txt
python ${cDNA_CupcakePATH}/annotation/subsample.py --by pbgene --min_fl_count 2 --step 1000 --range "(1000,3000)" ${OUTDIR}/cDNA_Cupcake/${sample}.for_subsampling.txt.all.txt  > ${OUTDIR}/cDNA_Cupcake/${sample}.1to3k.by_pbgene.rarefaction.txt
python ${cDNA_CupcakePATH}/annotation/subsample.py --by refgene --min_fl_count 2 --step 1000 ${OUTDIR}/cDNA_Cupcake/${sample}.for_subsampling.txt.all.txt > ${OUTDIR}/cDNA_Cupcake/${sample}.rarefaction.by_refgene.min_fl_2.txt 
python ${cDNA_CupcakePATH}/annotation/subsample.py --by refisoform --min_fl_count 2 --step 1000 ${OUTDIR}/cDNA_Cupcake/${sample}.for_subsampling.txt.all.txt > ${OUTDIR}/cDNA_Cupcake/${sample}.rarefaction.by_refisoform.min_fl_2.txt 
python ${cDNA_CupcakePATH}/annotation/subsample_with_category.py --by refisoform --min_fl_count 2 --step 1000 ${OUTDIR}/cDNA_Cupcake/${sample}.for_subsampling.txt.all.txt > ${OUTDIR}/cDNA_Cupcake/${sample}.rarefaction.by_refisoform.min_fl_2.by_category.txt
Rscript --vanilla ${cDNA_CupcakePATH}/plot_rarefaction.R ${OUTDIR} ${sample}

echo "10. Run again SQANTI3 annotation with isoAnnotLite option, tappasLike gff3 and polyAPeak (Aseq2 bed) as input"
mkdir -p ${OUTDIR}/SQANTI3_polyAPeak_2
python ${SQANTI3_PATH}/sqanti3_qc.py -o ${sample} -g --aligner_choice minimap2 -d ${OUTDIR}/SQANTI3_2  --polyA_motif_list ${POLYA_MOTIF} -fl ${OUTDIR}/${sample}.ccs.flnc.clustered.aligned.sorted.collapsed.abundance.txt --cage_peak $CAGE --genename --isoAnnotLite --gff3 ${OUTDIR}/SQANTI3_2/${sample}_tappAS_annot_from_SQANTI3.gff3 ${OUTDIR}/${sample}.ccs.flnc.clustered.aligned.sorted.collapsed.gtf $GTF $GENOME
